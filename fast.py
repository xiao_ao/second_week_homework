import cv2 as cv
import numpy as np

img_path = r'C:\Users\Administrator\Desktop\python\AI\material\chessboard.png'
#原图
ori = cv.imread(img_path)

#灰度图
img = cv.imread(img_path,cv.IMREAD_GRAYSCALE)


#fast = cv.FastFeatureDetector_create(threshold=40,nonmaxSuppression= True ,type= cv.FAST_FEATURE_DETECTOR_TYPE_9_16)
fast = cv.FastFeatureDetector_create()
img_bk = np.copy(img)

img_bk[::] = 255
img_bk_1 = img_bk.copy()
keypoints = fast.detect(img, None)
print('检测点数:')
print(len(keypoints))


res = cv.drawKeypoints(img_bk, keypoints, img_bk_1, color=(0,0,255),flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#直接显示只能显示一部分，先创建一个可调节大小的窗口
cv.namedWindow('fast',cv.WINDOW_NORMAL)
cv.imshow('fast',res)
